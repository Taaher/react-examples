import React from "react";
import Counter from "./components/Counter";
import Dogs from "./components/Dogs";
import Posts from "./components/Posts";

function App() {
  return (
    <div className='App'>
      <Counter />
      <hr />
      <Posts />
      <hr />
      <Dogs />
    </div>
  );
}

export default App;
