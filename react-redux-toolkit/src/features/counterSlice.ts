import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IInitialState {
  value: number;
}

const initialState: IInitialState = {
  value: 0,
};

const counterSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    amountAdded: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
  },
});
export const { increment, decrement, amountAdded } = counterSlice.actions;
export default counterSlice.reducer;
