import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchPosts = createAsyncThunk("fetch/post", async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await response.json();
  return data;
});

export interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}
export interface IInitialState {
  posts: IPost[];
  loading: boolean;
  errorMessage?: string | null;
}
const initialState: IInitialState = {
  posts: [],
  loading: false,
  errorMessage: null,
};
const postSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      state.posts = action.payload;
      state.loading = false;
    });
    builder.addCase(fetchPosts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchPosts.rejected, (state, action) => {
      console.log({ ERROR: action.payload });
      state.loading = false;
      state.errorMessage = "Something went wrong...";
    });
  },
});

export default postSlice.reducer;
