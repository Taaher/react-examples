import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const DOGS_API_KEY = "56aa0e13-bcee-4dc5-9ee3-1c701107a7eb";

interface Breed {
  id: string;
  name: string;
  image: {
    url: string;
  };
}
export const dogApiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://api.thedogapi.com/v1",
    prepareHeaders(headers) {
      headers.set("x-api-key", DOGS_API_KEY);

      return headers;
    },
  }),
  endpoints(builder) {
    return {
      fetchBreeds: builder.query<Breed[], number | void>({
        query(limit = 10) {
          return `/breeds?limit=${limit}`;
        },
      }),
    };
  },
});
// export const dogApiSlice = createApi({
//   reducerPath: "api",
//   baseQuery: fetchBaseQuery({
//     baseUrl: "https://api.thedogapi.com/v1",
//     prepareHeaders(headers) {
//       headers.set("x-api-key", DOGS_API_KEY);
//       return headers;
//     },
//   }),
//   endpoints(builder) {
//     return {
//       fetchBreeds: builder.query<Breed[], number | void>({
//         query(limit = 10) {
//           return `/breed?limit=${limit}`;
//         },
//       }),
//     };
//   },
// });

export const { useFetchBreedsQuery } = dogApiSlice;
