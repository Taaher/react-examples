import React from "react";
import { useAppDispatch, useAppSelector } from "../hooks";
import { fetchPosts } from "../features/postSlice";

export default function Posts() {
  const dispatch = useAppDispatch();
  const { posts, loading, errorMessage } = useAppSelector(
    (state) => state.posts
  );

  React.useEffect(() => {
    dispatch(fetchPosts());
  }, []);

  if (loading) return <h2>Loading!...</h2>;
  if (errorMessage) return <h2>{errorMessage}</h2>;

  return (
    <div>
      {posts &&
        posts.map((post) => {
          return (
            <div key={post.id}>
              <h2>{post.title}</h2>
            </div>
          );
        })}
    </div>
  );
}
