import { useAppDispatch, useAppSelector } from "../hooks";
import { increment, decrement, amountAdded } from "../features/counterSlice";
import React from "react";

export default function Counter() {
  const dispatch = useAppDispatch();
  const counter = useAppSelector((state) => state.counter.value);
  const refValue = React.useRef<HTMLInputElement>(null);

  const handleIncrement = () => dispatch(increment());
  const handleDecrement = () => dispatch(decrement());

  const addIncrement = () => {
    if (refValue.current?.value) {
      dispatch(amountAdded(parseInt(refValue.current?.value)));
    } else {
      //default add amount -> number 2
      dispatch(amountAdded(2));
    }
  };
  return (
    <div>
      <button onClick={handleIncrement}>Increment</button>
      <input ref={refValue} type='number' placeholder='type or default add 2' />
      <button onClick={addIncrement}>Number added</button>
      <h2> {counter} </h2>
      <button onClick={handleDecrement}>decrement</button>
    </div>
  );
}
