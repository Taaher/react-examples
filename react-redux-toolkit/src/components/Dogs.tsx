import { useFetchBreedsQuery } from "../features/dogApiSlice";

export default function Dogs() {
  const { data = [], isFetching } = useFetchBreedsQuery();
  if (isFetching) return <h2>Loading!...</h2>;
  return (
    <div>
      <h2>{data.length}</h2>
      <p>Number of dogs fetched: {data.length}</p>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {data.map((breed) => (
            <tr key={breed.id}>
              <td>{breed.name}</td>
              <td>
                <img
                  src={breed.image.url}
                  alt={breed.name}
                  height={250}
                  width={400}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
