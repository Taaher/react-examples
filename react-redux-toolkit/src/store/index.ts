import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counterSlice";
import postReducer from "../features/postSlice";
import { dogApiSlice } from "../features/dogApiSlice";

const store = configureStore({
  reducer: {
    counter: counterReducer,
    posts: postReducer,
    [dogApiSlice.reducerPath]: dogApiSlice.reducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(dogApiSlice.middleware);
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
