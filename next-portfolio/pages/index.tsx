import type { NextPage } from 'next';
import Portfolio from '../components/portfolio';
import Hero from '../components/Hero';
import About from '../components/About';
import Contact from '../components/Contact';

const Home: NextPage = () => {
  return (
    <div className='divide-y divide-primary'>
      <Hero />
      <Portfolio />
      <About />
      <Contact />
    </div>
  );
};

export default Home;
