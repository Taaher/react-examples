import Image from 'next/image';

export default function Portfolio() {
  return (
    <div className='bg-hero-pattern p-8 bg-no-repeat bg-cover' id='portfolio'>
      <div className='container mx-auto m-24 grid-cols-2 gap-8 my-8 space-y-16'>
        <div className='col-span-1'>
          <h2>Portfolio</h2>
          <p className='text-2/1 font-bold text-gray-500'>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed,
            sapiente dicta aliquam nisi nulla repellendus facere, eligendi optio
            nihil dolores unde voluptatibus veritatis recusandae amet quidem
            omnis corporis tempore. Voluptate! Lorem ipsum dolor sit, amet
            consectetur adipisicing elit. Nulla nesciunt deserunt dolorem
            obcaecati error numquam magnam eum similique soluta voluptates
            veritatis quisquam amet ut, ex cum voluptatum illo repellat
            doloribus.
          </p>
        </div>
        <div className='col-span-2'>
          <div className='bg-portfolio-work-background bg-cover bg-no-repeat bg-center'>
            <Image
              className='rounded'
              src='/images/portfolio-1.webp'
              alt='portfolio'
              height={285}
              width={995}
            />
          </div>
          {/* <div className='bg-portfolio-work-background bg-cover bg-no-repeat'>
          <Image
          className='rounded'
          src='/images/portfolio-1.png'
          alt='portfolio'
          height={285}
          width={495}
          />
        </div> */}
        </div>
      </div>
    </div>
  );
}
