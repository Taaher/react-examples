export default function Contact() {
  return (
    <div
      className='h-screen p-8 my-60 bg-hero-pattern bg-no-repeat bg-cover'
      id='contact'
    >
      <div className='container mx-auto my-44 '>
        <h2 className='my-16'> Contact Me </h2>
        <p className='text-2xl text-gray-600'>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Adipisci a
          animi accusantium, iste error molestiae optio delectus ipsum harum
          modi earum hic ipsam! Maxime voluptatibus eos nemo distinctio a Lorem
          ipsum, dolor sit amet consectetur adipisicing elit. Rem sapiente quasi
          soluta cum tenetur? Quo maiores ea amet quasi iste. Provident ab
          placeat rem non porro numquam, eius eaque aspernatur.
        </p>
        <form className='flex flex-col self-center justify-center items-center py-8'>
          <label htmlFor='name' className='leading-7 text-sm text-gray-600'>
            Name
          </label>
          <input
            type='text'
            id='name'
            name='name'
            className='w-full tablet:w-[50%] bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out'
          />
          <label htmlFor='Family' className='leading-7 text-sm text-gray-600'>
            Family
          </label>
          <input
            type='text'
            id='Family'
            name='Family'
            className='w-full tablet:w-[50%] bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out'
          />
          <label htmlFor='email' className='leading-7 text-sm text-gray-600'>
            Email
          </label>
          <input
            type='text'
            id='email'
            name='email'
            className='w-full tablet:w-[50%] bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out'
          />

          <label htmlFor='message' className='leading-7 text-sm text-gray-600'>
            Message
          </label>
          <textarea
            id='message'
            name='message'
            className='w-full tablet:w-[50%] bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out'
          ></textarea>
          <button className='mt-6 bg-pink-500 rounded py-2 px-4 w-[200px]  text-white hover:bg-pink-700'>
            Send
          </button>
        </form>
      </div>
    </div>
  );
}
