import Image from 'next/image';

export default function Hero() {
  return (
    <div className='bg-hero-pattern bg-cover bg-no-repeat bg-center h-screen'>
      <div className='container mx-auto flex justify-between items-center py-36 px-4 md:py-20'>
        <div className='flex flex-col'>
          <h1 className='font-bold'> I build awesome experiences </h1>
          <p className='mb-10 text-gray-600 text-2xl'>
            I am Web Developer | Ux Designer
          </p>
          <button className='bg-pink-500 rounded py-4 px-6 w-max  text-white hover:bg-pink-700'>
            Check out my work!
          </button>
        </div>
        <div>
          <Image
            src='/images/header-illustration.svg'
            alt='hero'
            height={500}
            width={500}
          />
        </div>
      </div>
    </div>
  );
}
