import Image from 'next/image';

export default function About() {
  return (
    <div className='container p-8 mx-auto my-40  ' id='about'>
      <h2 className='my-16'>About </h2>
      <div className='flex flex-col tablet:flex-row pt-10'>
        <div className='flex-1 bg-portfolio-work-background bg-cover bg-no-repeat bg-center'>
          <Image
            className='rounded'
            src='/images/about.webp'
            alt='portfolio'
            height={285}
            width={495}
          />
        </div>
        <p className='flex-1 text-2xl text-gray-600 px-8 mt-4'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto
          eaque tempore ut saepe dignissimos? Corrupti reprehenderit hic, nulla
          ab, soluta sed maiores quae assumenda placeat qui earum saepe! Harum,
          unde! Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Pariatur labore aperiam nemo aut. Soluta, possimus a reiciendis
          molestiae error nobis et voluptatibus, culpa earum magnam commodi
          architecto laboriosam cumque? A!
        </p>
      </div>
    </div>
  );
}
