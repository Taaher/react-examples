import Link from 'next/link';

export default function Navbar() {
  return (
    <header>
      <nav className='bg-secondary p-4 flex h-20 justify-between tablet:px-40  items-center'>
        <div className='font-bold text-4xl text-white'>TZ!</div>
        <ul className='flex space-x-4 font-bold text-white'>
          <li>
            <Link href='#portfolio'>
              <a>Portfolio</a>
            </Link>
          </li>
          <li>
            <Link href='#about'>
              <a>About</a>
            </Link>
          </li>
          <li>
            <Link href='#contact'>
              <a>Contact me</a>
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
