import Link from 'next/link';
import { RiGitlabFill, RiLinkedinBoxFill } from 'react-icons/ri';

export default function Footer() {
  return (
    <div className='flex   items-center p-4 h-24  space-x-4  shadow-inner mt-[500px]'>
      <div>
        <ul className='flex space-x-2'>
          <li>
            <Link href='https://gitlab.com/Taaher' passHref>
              <a>
                <RiGitlabFill size={30} color='orange' />
              </a>
            </Link>
          </li>
          <li>
            <Link href='https://www.linkedin.com/in/taherzobeydi/' passHref>
              <a>
                <RiLinkedinBoxFill
                  size={30}
                  color='white'
                  style={{ backgroundColor: '00001a' }}
                />
              </a>
            </Link>
          </li>
        </ul>
      </div>
      <div>
        <h4> Demo Portfolio by Taher</h4>
      </div>
    </div>
  );
}
