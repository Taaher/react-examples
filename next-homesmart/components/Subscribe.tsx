import Link from 'next/link'

export default function Subscribe() {
  return (
    <section className="relative ">
      <div className="flex flex-wrap-reverse justify-center gap-8 ">
        <div className="flex flex-col flex-wrap items-start lg:justify-center">
          <div>
            <div className="item-start flex flex-col flex-wrap gap-2">
              <h2 className="text-5xl font-bold">Relax,your Home!</h2>
              <p className="max-w-md font-bold">
                All our supported hardware includes traditional inputs so you
                can integrate our products into your house in a way thats bes
                for everyone.
              </p>
            </div>
            <Link href="/">
              <a
                className="focus:visible:ring-4 mt-4 flex w-32 gap-2 self-center rounded-md bg-amber-400 py-2 px-8 shadow-xl ring-neutral-900 ring-offset-4  transition-shadow
            hover:shadow-none
            focus:outline-none
          dark:text-neutral-900
            xl:items-center
            xl:justify-center "
              >
                Sign Up
              </a>
            </Link>
          </div>
        </div>
        <img src="assets/table.png" alt="table" />
      </div>
      {/* <div
        className="absolute -right-2 -bottom-6 -z-10
        aspect-square
         rounded-full
          border-amber-400
           md:w-64
           md:border-8
            lg:w-96
             xl:max-w-lg
             "
      ></div> */}
      <div className="-z-10  mt-12 h-24 bg-teal-900 xl:-bottom-8 xl:mt-0 xl:h-48 xl:w-full "></div>
    </section>
  )
}
