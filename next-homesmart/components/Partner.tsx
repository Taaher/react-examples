export default function Partner() {
  return (
    <section className="grid place-items-center gap-8 py-28 text-center">
      <div className="grid gap-4">
        <h2 className="text-4xl font-bold text-amber-400">Our partners</h2>
        <p className="mx-auto w-full max-w-lg font-bold text-gray-600">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iure, natus
          totam. Beatae quod, animi cumque, quia placeat qui sapiente eum dolor,
          exercitationem assumenda nobis officia consequuntur numquam iusto sunt
          eius.
        </p>
        <div className="max.w-2x1 mx-auto flex flex-wrap justify-center gap-8 md:gap-x-16">
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner1.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner2.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner3.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner4.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner5.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner6.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
          <div className="p-4.bg-white.dark:bg-neutral-600 rounded-md shadow-sm dark:shadow-xl">
            <img
              src="/assets/partner7.svg"
              alt="partner"
              className="h-16 w-16"
            />
          </div>
        </div>
      </div>
    </section>
  )
}
