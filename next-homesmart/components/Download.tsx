import Link from 'next/link'
export default function Download() {
  return (
    <div>
      <Link href="/">
        <a>
          <span
            className="focus:visible:ring-4 
                xl:w-2/2
                my-12
                mx-auto
                flex
                w-max
                gap-2
                self-center
                rounded-md
                bg-amber-400
                py-2
                px-6
                shadow-xl
                ring-neutral-900
                ring-offset-4    
                transition-shadow
            hover:shadow-none
            focus:outline-none
            dark:text-neutral-900
                xl:items-center
                xl:justify-center
            "
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
              />
            </svg>{' '}
            Download the App
          </span>
        </a>
      </Link>
    </div>
  )
}
