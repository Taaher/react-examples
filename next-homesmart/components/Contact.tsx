import Link from 'next/link'

export default function Contact() {
  return (
    <section className="container mx-auto h-auto overflow-hidden py-48 px-8">
      <div className="flex flex-wrap justify-center gap-12 space-x-1 md:gap-6 lg:gap-20 ">
        <div className="relative md:max-w-sm md:flex-1">
          <img src="/assets/lamp.png" alt="lamp" className="mx-auto" />
          <Link href="/">
            <a className="py-8">
              <span
                className="focus:visible:ring-4 
                xl:w-2/2
                mx-auto
                mb-12
                mt-12
                flex
                w-max
                gap-2
                self-center
                rounded-md
                bg-amber-400
                py-2
                px-6
                shadow-xl
                ring-neutral-900
                ring-offset-4    
                transition-shadow
                hover:shadow-none
               focus:outline-none
             dark:text-neutral-900
               xl:items-center
               xl:justify-center
            "
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                  />
                </svg>
                Download the App
              </span>
            </a>
          </Link>
          <img
            src="/assets/app.svg"
            className="absolute left-1/2 hidden max-w-[12rem] -translate-x-1/2 drop-shadow-xl md:block xl:max-w-[12rem]"
            alt="app"
            width="240"
          />
        </div>
        <form className="relative my-4 w-full gap-8 rounded-lg border-8 border-neutral-900 bg-white p-6 dark:bg-neutral-800 md:my-12 md:max-w-lg md:flex-1 lg:my-16">
          <h2 className="my-6 text-2xl font-bold">Let's Connect</h2>
          <div className="relative">
            <label
              htmlFor="name"
              className="
                        text-small -translate-y-1/2 font-bold text-neutral-500 transition-all"
            >
              Your name
            </label>
            <input
              type="text"
              id="name"
              className="form-input w-full  rounded-md border-4 border-amber-400 placeholder-transparent focus:border-amber-400 focus:outline-none focus:ring-2  focus:ring-offset-2 dark:bg-amber-400  dark:text-neutral-900 "
              placeholder="Your Name"
            />
          </div>
          <div className="relative my-2">
            <label
              htmlFor="name"
              className="
                        text-small   font-bold text-neutral-500 transition-all"
            >
              Email
            </label>
            <input
              type="text"
              id="email"
              className="form-input  w-full  rounded-md border-4 border-amber-400  focus:border-amber-400 focus:outline-none focus:ring-2  focus:ring-offset-2 dark:bg-amber-400  dark:text-neutral-900 "
            />
          </div>
          <div className="relative my-2">
            <label
              htmlFor="name"
              className="
                        text-small   font-bold text-neutral-500 transition-all"
            >
              How Can We Help?
            </label>
            <textarea
              id="email"
              cols={20}
              rows={5}
              className="form-textarea w-full resize-none  rounded-md border-4 border-amber-400  focus:border-amber-400 focus:outline-none focus:ring-2  focus:ring-offset-2 dark:bg-amber-400  dark:text-neutral-900 "
            ></textarea>
          </div>
          <Link href="/">
            <a
              className="
              focus:visible:ring-4
                mt-4             
                flex
                w-32
                gap-2
                self-center
                rounded-md  
                bg-neutral-900
                py-2
                px-8
                text-white
                shadow-xl
                ring-neutral-900
                ring-offset-4    
                transition-shadow
               hover:shadow-none
               focus:outline-none
             dark:text-neutral-900
               xl:items-center
               xl:justify-center
            "
            >
              Sign Up
            </a>
          </Link>
        </form>
      </div>
    </section>
  )
}
