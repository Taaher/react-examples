import Footer from './Footer'
import Navbar from './Navbar'

type LayoutProps = {
  children: React.ReactNode
}

export default function Layout({ children }: LayoutProps) {
  return (
    <div className="mx-auto max-w-[2000px] bg-white text-neutral-900 dark:bg-neutral-800  dark:text-neutral-200">
      <Navbar />
      {children}
      <Footer />
    </div>
  )
}
