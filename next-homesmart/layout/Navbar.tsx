import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import NavItems from './NavItems'
import Sidebar from './Sidebar'

export default function Navbar() {
  const [isOpen, setIsOpen] = useState<Boolean>(false)
  return (
    <div className="mx-auto flex items-center justify-between bg-amber-400 p-6">
      <nav className="container mx-auto ">
        <ul className="flex items-center justify-between">
          <li>
            <Link href="/">
              <a
                className="
                    z-50
                    rounded-sm
                    ring-offset-4
                    ring-offset-amber-400
                    lg:absolute
                    lg:left-1/2
                    lg:top-9
                    lg:-translate-x-1/2
                "
              >
                <Image
                  src="/assets/logo.svg"
                  alt="Home Smart Logo"
                  width={200}
                  height={40}
                  className="w-48 cursor-pointer md:w-64 lg:w-72"
                />
              </a>
            </Link>
          </li>
          <li>
            <button onClick={() => setIsOpen(!isOpen)} className="lg:hidden">
              {!isOpen ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h6a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                    clipRule="evenodd"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              )}
            </button>
          </li>
          {isOpen ? <Sidebar isOpen={isOpen} /> : null}
          <NavItems />
        </ul>
      </nav>
    </div>
  )
}
