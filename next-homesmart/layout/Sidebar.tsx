import Link from 'next/link'
type sidebarProps = {
  isOpen: Boolean
}
export default function Sidebar({ isOpen }: sidebarProps) {
  return (
    <ul
      className={
        isOpen
          ? 'absolute right-0 left-0 top-16 z-40 flex flex-col gap-4 bg-amber-400 p-6 text-center text-lg  shadow-xl lg:hidden'
          : `hidden`
      }
    >
      <li>
        <Link href="/">
          <a className="py-1 px-6 dark:text-neutral-900">Home</a>
        </Link>
      </li>
      <li>
        <Link href="/">
          <a className="py-1 px-6 dark:text-neutral-900">Contact</a>
        </Link>
      </li>
      <li>
        <Link href="/">
          <a
            className="
               py-1 px-6 dark:text-neutral-900"
          >
            Login
          </a>
        </Link>
      </li>
      <li>
        <Link href="/">
          <a
            className="
                
                 rounded-md
                bg-teal-900
                py-1
                px-6
                text-white
                shadow-xl
                transition-shadow hover:shadow-none dark:text-neutral-900"
          >
            Sign Up
          </a>
        </Link>
      </li>
    </ul>
  )
}
