import Link from 'next/link'

export default function NavItems() {
  return (
    <ul className="hidden justify-between py-1 px-6 font-bold ring-offset-amber-400 hover:text-neutral-600 lg:flex lg:w-full">
      <li>
        <Link href="/">
          <a className="py-1 px-6 dark:text-neutral-900">Home</a>
        </Link>
      </li>
      <li className="lg:mr-auto">
        <Link href="/">
          <a className="py-1 px-6 dark:text-neutral-900">Contact</a>
        </Link>
      </li>
      <li>
        <Link href="/">
          <a className="mr-auto py-1 px-6 dark:text-neutral-900">Login</a>
        </Link>
      </li>
      <li>
        <Link href="/">
          <a
            className="
                
                 rounded-md
                bg-teal-900
                py-1
                px-6
                text-white
                shadow-xl
                transition-shadow hover:shadow-none dark:text-neutral-900"
          >
            Sign Up
          </a>
        </Link>
      </li>
    </ul>
  )
}
