import Link from 'next/link'
import { RiGitlabFill, RiLinkedinBoxFill } from 'react-icons/ri'

export default function Footer() {
  return (
    <div className="mt-40 flex h-24 items-center  space-x-4 bg-amber-400 p-4 shadow-inner">
      <div>
        <ul className="flex space-x-2">
          <li>
            <Link href="https://gitlab.com/Taaher" passHref>
              <a>
                <RiGitlabFill size={30} color="orange" />
              </a>
            </Link>
          </li>
          <li>
            <Link href="https://www.linkedin.com/in/taherzobeydi/" passHref>
              <a>
                <RiLinkedinBoxFill
                  size={30}
                  color="white"
                  style={{ backgroundColor: '00001a' }}
                />
              </a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  )
}
