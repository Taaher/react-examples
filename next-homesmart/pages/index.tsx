import type { NextPage } from 'next'
import Contact from '../components/Contact'

import Content from '../components/Content'
import Hero from '../components/Hero'
import Partner from '../components/Partner'
import Subscribe from '../components/Subscribe'

const Home: NextPage = () => {
  return (
    <>
      <Hero />
      <Content />
      <Partner />
      <Subscribe />
      <Contact />
    </>
  )
}

export default Home
