export default function SecretPage() {
  const status = false;
  if (status) {
    return {
      redirect: { destination: '/' },
    };
  }
  return (
    <div>
      <h2> SecretPage is Ready! </h2>
    </div>
  );
}
