import Link from 'next/link';
import styles from './Navbar.module.scss';
import { useSession, signIn, signOut } from 'next-auth/react';

export default function Navbar() {
  const { data: session } = useSession();

  return (
    <nav className={styles.navbar}>
      <ul className={styles.menu}>
        <li className={styles.item}>
          <Link href='/'>Home</Link>
        </li>
        <li className={styles.item}>
          <Link href='/secret'>Secret</Link>
        </li>
        <li className={styles.item}>
          <Link href='/blog'>Blog</Link>
        </li>
        <li className={styles.item}>
          <Link href='/api/auth/signin'>
            <a
              onClick={(e: any) => {
                e.preventDefault();
                signIn();
              }}
            >
              Sign In
            </a>
          </Link>
        </li>
        <li className={styles.item}>
          <Link href='/api/auth/signout'>
            <a
              onClick={(e: any) => {
                e.preventDefault();
                signOut();
              }}
            >
              Sign out
            </a>
          </Link>
        </li>
        {session?.user?.email} <br />
        <li className={styles.item}>
          <button onClick={() => signOut()}>Sign out</button>
        </li>
      </ul>
    </nav>
  );
}
