import { METHODS } from 'http';
import { NextApiRequest, NextApiResponse } from 'next';
import { comments } from '../../../data/comments';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { commentId } = req.query;
  if (req.method === 'GET') {
    const comment = comments.find((comment: any) => comment.id === +commentId);
    return res.status(200).json(comment);
  } else if (req.method === 'DELETE') {
    const deleteComment = comments.find(
      (comment: any) => comment.id === +commentId
    );
    const index = comments.findIndex((comment) => comment.id === +commentId);
    comments.splice(index, 1);
    return res.status(200).json(deleteComment);
  }
}
