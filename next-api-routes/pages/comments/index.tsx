import { useState } from 'react';
import axios from 'axios';
import CommentList from '../../components/comments/commentList/CommentList';
import CreateComment from '../../components/comments/createComment/CreateComment';
import FetchComment from '../../components/comments/fetchComment/FetchComment';

type Comment = {
  id: number;
  text: String;
};

export default function CommentPage() {
  const [comments, setComments] = useState<Comment[]>([]);
  const [loading, setLoading] = useState<Boolean>(false);
  const [HasError, setHasError] = useState<Error | undefined>();

  const [newComment, setNewComment] = useState();

  //load data from api
  const fetchComments = async () => {
    setLoading(true);
    axios
      .get('/api/comments')
      .then((res) => {
        setLoading(false);
        setComments(res.data);
      })
      .catch((err) => setHasError(err));
  };
  //set error page!
  if (HasError) return <h2>Sorry Something wrong</h2>;

  //set Loading page!
  if (loading) return <h2>Loading!...</h2>;

  const onSubmit = async (e: any) => {
    e.preventDefault();
    await axios
      .post('/api/comments', {
        newComment,
      })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
    console.log(newComment);
  };

  const removeComment = async (id: number) => {
    await axios
      .delete(`/api/comments/${id}`)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
    fetchComments();
  };
  return (
    <div>
      <CreateComment onSubmit={onSubmit} setNewComment={setNewComment} />
      <FetchComment fetchComments={fetchComments} />
      <CommentList comments={comments} removeComment={removeComment} />
    </div>
  );
}
