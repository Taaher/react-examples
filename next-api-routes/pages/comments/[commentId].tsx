import DetailComment from '../../components/comments/detailComment/DetailComment';
import { comments } from '../../data/comments';

export default function CommentDetail({ comment }: any) {
  return <DetailComment id={comment?.id} text={comment?.text} />;
}

export async function getStaticPaths() {
  return {
    //all address!
    paths: [],
    fallback: true,
  };
}

export async function getStaticProps(context: any) {
  const { params } = context;
  const { commentId } = params;
  //fetch one comment
  // const comment = async () => {
  //   await axios
  //     .get(`/api/comments/${commentId}`)
  //     .then((res) => console.log(res))
  //     .catch((err) => console.log(err));
  // };

  const comment = comments.find((comment) => comment.id === +commentId);
  return {
    props: {
      comment,
    },
  };
}
