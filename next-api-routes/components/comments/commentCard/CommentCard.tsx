import RemoveComment from '../removeComment/RemoveComment';
import styles from './CommentCard.module.scss';

type Comment = {
  id: number;
  text: string;
  removeComment: Function;
};

export default function CommentCard({ id, text, removeComment }: Comment) {
  return (
    <div className={styles.container}>
      <div className={styles.card}>
        <h2>
          {id} - {text}
        </h2>
        <RemoveComment removeComment={removeComment} id={id} />
      </div>
    </div>
  );
}
