import styles from './FetchComment.module.scss';
export default function FetchComment({ fetchComments }: any) {
  return (
    <div className={styles.container}>
      <button onClick={fetchComments} className={styles.btn}>
        Fetch All Comments
      </button>
    </div>
  );
}
