import CommentCard from '../commentCard/CommentCard';
import RemoveComment from '../removeComment/RemoveComment';

type Comment = {
  id: number;
  text: String;
};

interface PropsComment {
  comments: Comment[];
  removeComment: Function;
}
export default function CommentList({ comments, removeComment }: PropsComment) {
  return (
    <div>
      {comments &&
        comments.map((comment: Comment) => {
          return (
            <div key={comment.id}>
              <CommentCard
                id={comment.id}
                text={comment.text}
                removeComment={removeComment}
              />
            </div>
          );
        })}
    </div>
  );
}
