import React from 'react';
import styles from './RemoveComment.module.scss';

interface IPropsRemoveComment {
  id: number;
  removeComment: (id: number) => void;
}

export default function RemoveComment({
  removeComment,
  id,
}: IPropsRemoveComment) {
  const removeItem = (id: any) => {
    console.log(typeof id);
    console.log(id);
    removeComment(id);
  };
  return (
    <button onClick={removeItem} className={styles.btn}>
      delete
    </button>
  );
}
