type Comment = {
  id: number;
  text: string;
};

export default function DetailComment({ id, text }: Comment) {
  return (
    <div className={''}>
      <div className={''}>
        <h2>
          {id} - {text}
        </h2>
      </div>
    </div>
  );
}
