import styles from './CreateComment.module.scss';

export default function CreateComment({ onSubmit, setNewComment }: any) {
  return (
    <div className={styles.container}>
      <form onSubmit={onSubmit}>
        <input
          className={styles.input}
          type='text'
          onChange={(e: any) => setNewComment(e.target.value)}
        />
        <button type='submit' className={styles.btn}>
          Send
        </button>
      </form>
    </div>
  );
}
