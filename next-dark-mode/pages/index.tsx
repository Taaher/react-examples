import ThemeChanger from '../components/ThemeChanger';

export default function IndexPage() {
  return (
    <div>
      <h2> IndexPage is Ready! </h2>
      <ThemeChanger />
    </div>
  );
}
