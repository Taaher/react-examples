/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: [
      "aceternity.com",
      "https://aceternity.com/images/products/thumbnails/new/moonbeam.png",
    ],
  },
  // eslint: {
  //   ignoreDuringBuilds: true,
  // },
};

export default nextConfig;
