"use client";

import Image from "next/image";
import { Tabs } from "../ui/tabs";
import { Suspense } from "react";
import PieChart from "./PieChart";
import { LineChart } from "./LineChart";
import BubbleChart from "./BubbleChart";
import AreaChart from "./AreaChart";

export default function Charts() {
  const tabs = [
    {
      title: "Line Chart",
      value: "product",
      content: (
        <div className="w-full overflow-hidden relative h-full rounded-2xl p-10 text-xl md:text-4xl font-bold  bg-gradient-to-br from-blue-100 to-slate-100">
          <Suspense fallback={<h2>loading...</h2>}>
            <LineChart />
          </Suspense>
        </div>
      ),
    },
    {
      title: "Pie Chart",
      value: "services",
      content: (
        <div className="w-full overflow-hidden relative h-full rounded-2xl p-10 text-xl md:text-4xl font-bold text-white bg-gradient-to-br from-blue-50 to-blue-100">
          <Suspense fallback={<h2>loading...</h2>}>
            <PieChart />
          </Suspense>
        </div>
      ),
    },
    {
      title: "Bubble Chart",
      value: "playground",
      content: (
        <div className="w-full overflow-hidden relative h-full rounded-2xl p-10 text-xl md:text-4xl font-bold text-white bg-gradient-to-br from-blue-50 to-violet-100">
          <Suspense fallback={<h2>loading...</h2>}>
            <BubbleChart />
          </Suspense>
        </div>
      ),
    },
    {
      title: "Area Charts",
      value: "content",
      content: (
        <div className="w-full overflow-hidden relative h-full rounded-2xl p-10 text-xl md:text-4xl font-bold text-white bg-gradient-to-br from-blue-100 to-violet-100">
          <Suspense fallback={<h2>loading...</h2>}>
            <AreaChart />
          </Suspense>
        </div>
      ),
    },
  ];

  return (
    <div id="charts" className="h-[20rem] md:h-[40rem] [perspective:1000px] relative b flex flex-col max-w-5xl mx-auto w-full  items-start justify-start my-40">
      <Tabs tabs={tabs} />
    </div>
  );
}

const DummyContent = () => {
  return (
    <Image
      src="/linear.webp"
      alt="dummy image"
      width="1000"
      height="1000"
      className="object-cover object-left-top h-[60%]  md:h-[90%] absolute -bottom-10 inset-x-0 w-[90%] rounded-xl mx-auto"
    />
  );
};
