"use client";

import React from "react";
import { HeroParallax } from "../ui/hero-parallax";

export default function About() {
  return <section id="about">
    <HeroParallax products={products} />
  </section>;
}
export const products = [
  {
    title: "Pie Chart",
    link: "https://visualdata.vercel.app",
    thumbnail: "/charts/pieChart.webp"
  },
  {
    title: "Bar Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/barChart.webp",
  },
  {
    title: "Bubble Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/bubbleChart.png",
  },

  {
    title: "Pie Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/pieChart2.png",
  },
  {
    title: "Pie Chart",
    link: "https://visualdata.vercel.app",
    thumbnail: "/charts/pieChart.webp"
  },
  {
    title: "Bar Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/barChart.webp",
  },
  {
    title: "Bubble Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/bubbleChart.png",
  },
  {
    title: "Pie Chart",
    link: "https://visualdata.vercel.app",
    thumbnail: "/charts/pieChart.webp"
  },
  {
    title: "Bar Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/barChart.webp",
  },
  {
    title: "Bubble Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/bubbleChart.png",
  },
  {
    title: "Pie Chart",
    link: "https://visualdata.vercel.app",
    thumbnail: "/charts/pieChart.webp"
  },
  {
    title: "Bar Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/barChart.webp",
  },
  {
    title: "Bubble Chart",
    link: "https://visualdata.vercel.app",
    thumbnail:
      "/charts/bubbleChart.png",
  },
];
