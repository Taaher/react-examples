'use client';
import React from "react";
import { TzIcon } from "../ui/TzIcon";
export default function Footer() {
  return (

    <footer className="h-[30rem] mt-96 w-full dark:bg-black bg-white dark:bg-grid-white/[0.4] bg-grid-black/[0.4]  relative flex flex-col items-center justify-center  ">
      {/* dotted background! */}
      {/* <footer className="h-[30rem] mt-96 w-full dark:bg-black bg-white dark:bg-dot-white/[0.2] bg-dot-black/[0.2]  relative flex flex-col items-center justify-center  "> */}
      {/* Radial gradient for the container to give a faded look */}
      <div className="absolute pointer-events-none inset-0 flex items-center justify-center dark:bg-black bg-white [mask-image:radial-gradient(ellipse_at_center,transparent_20%,black)]"></div>

      <div className="w-full h-full flex justify-center items-center flex-col">
        <p className="text-3xl sm:text-6xl font-bold relative z-20 bg-clip-text text-transparent bg-gradient-to-b from-neutral-200 to-neutral-500 py-8">
          Thank You For Watching.
        </p>
      </div>
      <div className="w-full flex justify-end p-4">
        <p className="text-sm sm:text-sm font-bold relative bg-clip-text text-transparent bg-gradient-to-b from-gray-200 to-gray-500">
          <TzIcon className="h-6 w-6 inline mb-1" /> Designed by Taher zobeydi
        </p>
      </div>
    </footer>
  );
}
