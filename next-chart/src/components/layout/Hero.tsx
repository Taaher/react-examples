'use client';
import { WavyBackground } from "../ui/wavy-background";
import { useTheme } from "next-themes";
import { useEffect } from 'react';

export default function Hero() {
  const { theme, systemTheme } = useTheme();


  const getBackgroundFill = () => {
    if (theme === "system") {
      return systemTheme === "dark" ? "#000" : "#ffffff";
    } else {
      return theme === "dark" ? "#000" : "#ffffff";
    }
  };

  return (
    <WavyBackground id="home" className="mx-auto max-w-4xl pb-40" backgroundFill={getBackgroundFill()}>
      <p className="inter-var text-center text-2xl font-bold md:text-4xl lg:text-5xl">
        Data in Motion: Exploring the Art of Visualization
      </p>
      <p className="inter-var mt-4 text-center text-base font-normal md:text-lg">
        Decoding Data: Visualizing Patterns for Actionable Insights
      </p>
    </WavyBackground>
  );
}
