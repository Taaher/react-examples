'use client';
export function TzIcon(props: any) {
  return (
    <svg  {...props} width="15" height="15" viewBox="0 0 685 526" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M116.746 124.13V136.182H176.164H235.582V124.13V112.079H176.164H116.746V124.13Z" fill="#0808F0" />
      <path d="M433.175 410.027V422.356H511.728H590.281V410.027V397.698H511.728H433.175V410.027Z" fill="#0808F0" />
      <path d="M332.426 449.5V462H480.004H627.581V449.5V437H480.004H332.426V449.5Z" fill="#0808F0" />
      <rect x="600.567" y="397.698" width="26.8338" height="24.1029" fill="#0808F0" />
      <rect x="486.842" y="112.079" width="26.8338" height="24.1029" fill="#0808F0" />
      <path d="M292.132 124V136H382.29H472.449V124V112H382.29H292.132V124Z" fill="#0808F0" />
      <path d="M304.779 203.67H317.557V157.874V112.079H304.779H292.001V157.874V203.67H304.779Z" fill="#0808F0" />
      <rect x="395.578" y="60.2573" width="26.8338" height="25.3081" fill="#0808F0" />
      <rect x="292.001" y="60.2573" width="95.8351" height="25.3081" fill="#0808F0" />
      <rect x="292.001" y="437.468" width="26.8338" height="24.1029" fill="#0808F0" />
      <rect x="77.1346" y="112.079" width="26.8338" height="24.1029" fill="#0808F0" />
      <rect x="249.638" y="60.2573" width="26.8338" height="185.593" fill="#0808F0" />
      <rect x="292.042" y="221.106" width="26.8377" height="200.159" fill="#0808F0" />
      <rect x="276.472" y="60.2573" width="25.3081" height="199.337" transform="rotate(90 276.472 60.2573)" fill="#0808F0" />
      <rect x="627.401" y="60.2573" width="25.3081" height="195.504" transform="rotate(90 627.401 60.2573)" fill="#0808F0" />
      <rect width="26.2293" height="357.17" transform="matrix(0.790337 0.612673 -0.657006 0.753886 606.779 70)" fill="#0808F0" />
      <rect width="26.2402" height="324.283" transform="matrix(0.794728 0.606966 -0.65143 0.758709 543.674 192)" fill="#0808F0" />
      <rect x="319.331" y="221" width="25" height="69.5074" transform="rotate(90 319.331 221)" fill="#0808F0" />
      <rect x="249.908" y="256.696" width="26.8338" height="204.875" fill="#0808F0" />
    </svg>

  );
}
