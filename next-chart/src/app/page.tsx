import About from "@/components/about/About";
import Charts from "@/components/charts/Charts";
import Hero from "@/components/layout/Hero";

export default function Home() {
  return (
    <main >
      {/* <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex"> */}
      <Hero />
      <About />
      <Charts />
      {/* </div> */}
    </main>
  );
}
