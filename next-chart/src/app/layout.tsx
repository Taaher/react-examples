import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ThemeProvider } from "@/components/layout/theme-provider";
import "./globals.css";
//layout
import Navbar from "@/components/layout/Navbar";
import Footer from '@/components/layout/Footer';
//font default
const inter = Inter({ subsets: ["latin"] });
//meta tag and description for seo.
export const metadata: Metadata = {
  title: "TZ | Data Visualization",
  description: "Generated Web Data Visualizations by Taher zobeydi",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" suppressHydrationWarning={true} className="scroll-smooth" >
      <body className={inter.className} suppressHydrationWarning={true}>
        <ThemeProvider
          attribute="class"
          defaultTheme="system"
          enableSystem
          disableTransitionOnChange
        >
          <Navbar />
          {children}
          <Footer />
        </ThemeProvider>
      </body>
    </html>
  );
}
