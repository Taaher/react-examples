import Image from 'next/image';

export default function Analytics() {
  return (
    <div className='bg-white w-full  py-16 px-4'>
      <div className='max-w-[1240px] mx-auto grid md:grid-cols-2'>
        <Image
          src='/images/analyst.webp'
          alt='analyst'
          height={500}
          width={500}
        />
        <div className='flex flex-col justify-center space-y-6 py-4 px-8'>
          <p className='text-green-600 font-bold'>DATA ANALYTICS DASHBOARD</p>
          <h1 className='font-bold underline'>
            Manage Data Analytics Centrally
          </h1>
          <p className='text-[20px] font-bold'>
            A financial data analyst prepares financial reports that serve as
            summary information for managers. The financial data analyst is
            responsible for identifying relevant insights and compiling
            analytical reports that enable other employees in the company to
            make sound decisions.
          </p>
          <button className='bg-green-400 p-3 w-40 rounded self-center'>
            GET START
          </button>
        </div>
      </div>
    </div>
  );
}
