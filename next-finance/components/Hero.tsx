import { useRef } from 'react';
import ReactTyped from 'react-typed';

export default function Hero() {
  const typedRef = useRef();
  return (
    <div className='bg-black text-white '>
      <div className='max-w-[800px]  h-screen w-full mx-auto text-center flex flex-col justify-center'>
        <p className='text-sm text-green-300 uppercase font-bold p-2'>
          growing with data Analytics
        </p>
        <h2 className='text-[50px] font-bold p-2 md:text-4x1'>
          Grow with data.
        </h2>
        <div>
          <p className='text-2xl md:text-5xl'>
            Fast,Flexible,Financing for {''}
            <ReactTyped
              loop
              typeSpeed={250}
              backSpeed={250}
              strings={['BTB', 'BTC', 'SASS']}
            />
          </p>
        </div>
        <p className='p-4 text-gray-600 text-lg font-bold'>
          Monitor your data analytics to increase revenue for BTB,BTC,& SASS
          Platforms
        </p>
        <button className='bg-green-400 p-3 w-40 rounded self-center'>
          GET START
        </button>
      </div>
    </div>
  );
}
