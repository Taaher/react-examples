import Image from 'next/image';

export default function Cards() {
  return (
    <div className='w-full py-[10rem] px-14 bg-white'>
      <div className='mx-w-[1240px] mx-auto grid md:grid-cols-3 gap-8'>
        <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg'>
          <div className='self-center'>
            <Image
              src='/images/single.png'
              alt='single'
              height={40}
              width={40}
            />
          </div>
          <h2 className='text-2xl font-bold text-center py-8'>Single User</h2>
          <p className='font-bold text-center text-4xl'>$149</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>500 GB Storage</p>
            <p className='py-2 border-b mx-8 mt-8'>1 Granted User</p>
            <p className='py-2 border-b mx-8 mt-8'>Send Up to 2 GB</p>
          </div>
          <button className='bg-green-400 p-3 w-40 rounded self-center my-8'>
            Send Trial
          </button>
        </div>
        {/* double user */}
        <div className='w-full shadow-xl flex flex-col p-4 my-10 rounded-lg bg-gray-100'>
          <div className='self-center'>
            <Image
              src='/images/double.png'
              alt='single'
              height={40}
              width={40}
            />
          </div>
          <h2 className='text-2xl font-bold text-center py-8'>Double User</h2>
          <p className='font-bold text-center text-4xl'>$199</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>1 TB Storage</p>
            <p className='py-2 border-b mx-8 mt-8'>3 Users Granted</p>
            <p className='py-2 border-b mx-8 mt-8'>Send Up to 10 GB</p>
          </div>
          <button className='bg-black text-green-400 p-3 w-40 rounded self-center my-8'>
            Send Trial
          </button>
        </div>
        {/* triple user */}
        <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg'>
          <div className='self-center'>
            <Image
              src='/images/triple.png'
              alt='single'
              height={40}
              width={40}
            />
          </div>
          <h2 className='text-2xl font-bold text-center py-8'>Triple User</h2>
          <p className='font-bold text-center text-4xl'>$299</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>5 TB Storage</p>
            <p className='py-2 border-b mx-8 mt-8'>10 Users Granted</p>
            <p className='py-2 border-b mx-8 mt-8'>Send Up to 2 GB</p>
          </div>
          <button className='bg-green-400 p-3 w-40 rounded self-center my-8'>
            Send Trial
          </button>
        </div>
      </div>
    </div>
  );
}
