export default function NewsLetter() {
  return (
    <div className='w-full py-20 text-white bg-black'>
      <div className='max-w-[1240px] mx-auto grid lg:grid-cols-3'>
        <div className='col-span-2'>
          <h1 className='text-4xl font-bold px-4'>
            Want tips & tricks to optimize your flow?
          </h1>
          <p className='p-4 text-gray-400'>
            Sign up to our newsletter and stay up to date.
          </p>
        </div>
        <div className='col-span-1 flex flex-col '>
          <div className='flex flex-col space-x-2 md:flex-row '>
            <input
              placeholder='Enter Email'
              type='email'
              className='h-12 w-[400px] rounded flex ml-2 '
            />
            <button className='bg-green-400  sm:m-0 h-12 w-40 rounded m-2 '>
              Notify Me
            </button>
          </div>
          <p className='text-bold p-4  text-md text-gray-400'>
            We care about the protection of your data. Read our{' '}
            <span className='text-green-400'>Privacy Policy</span>
          </p>
        </div>
      </div>
    </div>
  );
}
