import type { NextPage } from 'next';
import Head from 'next/head';
import Analytics from '../components/Analytics';
import Cards from '../components/Cards';
import Hero from '../components/Hero';
import NewsLetter from '../components/NewsLetter';

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Finance!</title>
      </Head>
      <Hero />
      <Analytics />
      <NewsLetter />
      <Cards />
    </>
  );
};

export default Home;
