import Link from 'next/link';

export default function Sidebar() {
  return (
    <nav>
      <ul className='flex-col space-y-6 p-12 text-xl fixed left-0 top-0 w-[60%] h-full bg-black text-white'>
        <li className='block border-b p-4 border-gray-800 hover:underline underline-offset-4 decoration-green-300 '>
          <Link href='/'>
            <a>Home</a>
          </Link>
        </li>
        <li className='block border-b p-4 border-gray-800 hover:underline underline-offset-4 decoration-green-300'>
          <Link href='/'>
            <a>Company</a>
          </Link>
        </li>
        <li className='block border-b p-4 border-gray-800 hover:underline underline-offset-4 decoration-green-300'>
          <Link href='/'>
            <a>Resources</a>
          </Link>
        </li>
        <li className='block border-b p-4 border-gray-800 hover:underline underline-offset-4 decoration-green-300'>
          <Link href='/'>
            <a>Contact</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
}
