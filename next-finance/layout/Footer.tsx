import Image from 'next/image';
import Link from 'next/link';
import {
  RiInstagramLine,
  RiLinkedinBoxFill,
  RiGitlabFill,
} from 'react-icons/ri';

export default function Footer() {
  return (
    <div className='w-full bg-black'>
      <footer className='max-w-[1240px] mx-auto py-16 grid lg:grid-cols-3 gap-8 text-gray-300'>
        <div className=''>
          <h1 className='font-bold text-2xl text-green-300 flex'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              className='h-5 w-5'
              viewBox='0 0 20 20'
              fill='currentColor'
            >
              <path d='M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z' />
              <path d='M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z' />
            </svg>
            Finance!{' '}
          </h1>
          <p className='py-2 text-gray-400 text-md font-bold'>
            A financial data analyst prepares financial reports that serve as
            summary information for managers. The financial data analyst is
            responsible for identifying relevant insights and compiling
            analytical reports that enable other employees in the company to
            make sound decisions.
          </p>
          <div className='flex space-x-4'>
            <Image
              src='/images/tz.svg'
              height={30}
              className='flex justify-start'
              width={30}
              alt='logo'
              priority
            />
            <Link href='https://gitlab.com/Taaher' passHref>
              <a>
                <RiGitlabFill size={30} color='orange' />
              </a>
            </Link>
            <Link href='https://www.linkedin.com/in/taherzobeydi/' passHref>
              <a>
                <RiLinkedinBoxFill
                  size={30}
                  color='white'
                  style={{ backgroundColor: '00001a' }}
                />
              </a>
            </Link>
          </div>
        </div>
        <div className='lg:col-span-2 flex justify-between px-4'>
          <div>
            <h6 className='font-medium text-gray-400'>Solutions</h6>
            <ul>
              <li className='py-2 text-sm'>Analytics</li>
              <li className='py-2 text-sm'>Marketing</li>
              <li className='py-2 text-sm'>Commerce</li>
              <li className='py-2 text-sm'>Insights</li>
            </ul>
          </div>
          <div>
            <h6 className='font-medium text-gray-400'>Supports</h6>
            <ul>
              <li className='py-2 text-sm'>Pricing</li>
              <li className='py-2 text-sm'>Documentation</li>
              <li className='py-2 text-sm'>Guides</li>
              <li className='py-2 text-sm'>Api Status</li>
            </ul>
          </div>
          <div>
            <h6 className='font-medium text-gray-400'>Company</h6>
            <ul>
              <li className='py-2 text-sm'>About</li>
              <li className='py-2 text-sm'>Blog</li>
              <li className='py-2 text-sm'>Jobs</li>
              <li className='py-2 text-sm'>Careers</li>
            </ul>
          </div>
          <div>
            <h6 className='font-medium text-gray-400'>Legal</h6>
            <ul>
              <li className='py-2 text-sm'>Claim</li>
              <li className='py-2 text-sm'>Policy</li>
              <li className='py-2 text-sm'>Term</li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  );
}
