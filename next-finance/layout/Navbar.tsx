import Link from 'next/link';
import { useState } from 'react';
import { isPromise } from 'util/types';
import Sidebar from './Sidebar';

export default function Navbar() {
  const [isOpen, setIsOpen] = useState<Boolean>(false);
  return (
    <div>
      <nav className='flex justify-between w-full  px-12 md:px-40 shadow-lx  border-gray-50 bg-black h-28 text-white items-center'>
        <h1 className='font-bold text-2xl text-green-300 flex'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='h-5 w-5'
            viewBox='0 0 20 20'
            fill='currentColor'
          >
            <path d='M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z' />
            <path d='M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z' />
          </svg>
          Finance!{' '}
        </h1>
        <ul className='hidden lg:flex space-x-4 text-xl'>
          <li className='block underline hover:underline underline-offset-4 decoration-green-300 '>
            <Link href='/'>
              <a>Home</a>
            </Link>
          </li>
          <li className='block hover:underline underline-offset-4 decoration-green-300'>
            <Link href='/'>
              <a>Company</a>
            </Link>
          </li>
          <li className='block hover:underline underline-offset-4 decoration-green-300'>
            <Link href='/'>
              <a>Resources</a>
            </Link>
          </li>
          <li className='block hover:underline underline-offset-4 decoration-green-300'>
            <Link href='/'>
              <a>About</a>
            </Link>
          </li>
          <li className='block hover:underline underline-offset-4 decoration-green-300'>
            <Link href='/'>
              <a>Contact</a>
            </Link>
          </li>
        </ul>
        <div className='lg:hidden'>
          <button onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
                strokeWidth={2}
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M6 18L18 6M6 6l12 12'
                />
              </svg>
            ) : (
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
                strokeWidth={2}
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M4 6h16M4 12h8m-8 6h16'
                />
              </svg>
            )}
          </button>
        </div>
      </nav>
      {/* sidebar */}
      <div>{isOpen ? <Sidebar /> : ''}</div>
    </div>
  );
}
