import { observer } from "mobx-react-lite";
import taskList, { FILTERS } from "../../mobx/TaskList";

function FilterTask() {
  return (
    <select
      value={taskList.filter}
      onChange={(e) => taskList.setFilter(e.target.value)}
    >
      <option value={FILTERS.ALL}>All</option>
      <option value={FILTERS.DONE}>Done</option>
      <option value={FILTERS.UNDONE}>UnDone</option>
    </select>
  );
}
export default observer(FilterTask);
