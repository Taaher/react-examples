import React from "react";
import { observer } from "mobx-react-lite";
import { Task } from "../../mobx/Task";
import taskList from "../../mobx/TaskList";

function CreateTask() {
  const [title, setTitle] = React.useState("");
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const newTask = new Task();
    newTask.setTitle(title);
    taskList.add(newTask);
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          value={title}
          placeholder='Type Task'
          onChange={(e) => setTitle(e.target.value)}
        />
      </form>
    </div>
  );
}
export default observer(CreateTask);
