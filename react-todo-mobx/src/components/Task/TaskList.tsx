import { observer } from "mobx-react-lite";
import taskList from "../../mobx/TaskList";

function TaskList() {
  const toggleTask = (task: any) => () => {
    task.toggle();
  };

  /*
    const innerFunction = toggleTask({})
    innerFunction()
    */
  return (
    <div>
      <ul>
        {taskList.filteredList.map((task: any) => {
          return (
            <div key={task.id}>
              <li>
                <input
                  type='checkbox'
                  checked={task.done}
                  onChange={toggleTask(task)}
                />
                {task.title}
              </li>
            </div>
          );
        })}
      </ul>
    </div>
  );
}

export default observer(TaskList);
