import React from "react";
import "./App.css";
import CreateTask from "./components/Task/CreateTask";
import FilterTask from "./components/Task/FilterTask";
import TaskList from "./components/Task/TaskList";

function App() {
  return (
    <div className='App'>
      <CreateTask />
      <FilterTask />
      <TaskList />
    </div>
  );
}

export default App;
