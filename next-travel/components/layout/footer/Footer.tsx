import Link from 'next/link'
import { RiGitlabFill, RiLinkedinBoxFill } from 'react-icons/ri'

export default function Footer() {
  return (
    <div className="flex items-center justify-center gap-2 bg-white p-7 text-center shadow-inner shadow-violet-50">
      <Link href="https://gitlab.com/Taaher" passHref>
        <a>
          <RiGitlabFill size={30} color="orange" />
        </a>
      </Link>

      <Link href="https://www.linkedin.com/in/taherzobeydi/" passHref>
        <a>
          <RiLinkedinBoxFill
            size={30}
            color="white"
            style={{ backgroundColor: '00001a' }}
          />
        </a>
      </Link>
      <h1 className="text-base font-semibold text-gray-800">Demo by Taher</h1>
    </div>
  )
}
