import { Disclosure } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'
import { navLinks } from './NavLinks'
import Link from 'next/link'

const classNames = (...classes: string[]) => classes.filter(Boolean).join(' ')

export default function Navbar() {
  return (
    <Disclosure as="nav" className="bg-white">
      {({ open }) => (
        <>
          <div className="min-w-7xl mx-auto border-b border-gray-50 bg-white px-2 sm:px-6 lg:px-8">
            <div className="relative mx-0 flex h-16 items-center justify-between md:mx-20">
              <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                <Disclosure.Button className="inline-flex items-center  justify-center rounded-md p-2 text-gray-400 hover:bg-violet-700 hover:text-white focus:outline-none focus:ring-inset focus:ring-white">
                  <span className="sr-only">Open Menu</span>
                  {open ? (
                    <XIcon className="h6 block w-6" aria-hidden="true" />
                  ) : (
                    <MenuIcon className="h6 block w-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>

              <div className=" flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                <div className="flex flex-shrink-0 items-center">
                  <h1 className="cursor-pointer text-xl font-semibold">
                    Travel<span className="text-violet-500">in</span>
                  </h1>
                </div>
                <div className="hidden sm:ml-6 sm:block md:ml-60">
                  <div>
                    <ul className="flex space-x-4">
                      {navLinks.map((item) => {
                        return (
                          <Link href={item.path} key={item.name}>
                            <li>
                              <a
                                className={classNames(
                                  item.current
                                    ? 'bg-violet-500 text-white shadow-lg'
                                    : 'text-gray-300 hover:bg-violet-500 hover:text-white hover:shadow-lg',
                                  'my-2 cursor-pointer rounded-md p-3 px-3 text-lg font-medium '
                                )}
                              >
                                {item.name}
                              </a>
                            </li>
                          </Link>
                        )
                      })}
                    </ul>
                  </div>
                </div>
              </div>
              <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                <button className="rounded-md border border-violet-100 px-3 py-2 text-sm font-medium text-violet-400 hover:bg-violet-500 hover:text-white">
                  Register
                </button>
              </div>
            </div>
          </div>
          <Disclosure.Panel className="sm:hidden">
            <div className="space-y-1 px-2 pt-2 pb-3">
              {navLinks.map((item) => {
                return (
                  <Disclosure.Button
                    key={item.name}
                    href={item.path}
                    as="a"
                    className={classNames(
                      item.current
                        ? 'bg-violet-500 text-white shadow-lg'
                        : ' text-gray-300 hover:bg-violet-500 hover:shadow-lg',
                      'block rounded-md px-3 py-2 text-base font-medium'
                    )}
                  >
                    {item.name}
                  </Disclosure.Button>
                )
              })}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  )
}
