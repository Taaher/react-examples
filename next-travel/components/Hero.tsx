import Image from 'next/image'
export default function Hero() {
  return (
    <>
      <div className="w-full">
        <div className="flex h-auto w-full items-center justify-center p-8">
          <div className="ml-10 md:ml-20 md:w-1/2">
            <h2 className="my-5 text-5xl font-bold text-gray-800 md:text-7xl">
              Explore World
            </h2>
            <p className="text-base font-bold text-gray-400 md:text-lg ">
              Traveling is one of the best way to enhance personal growth. It
              enables you to do things different from daily routine activities.
            </p>
            <div className="mt-12 flex items-start justify-start gap-5 text-center">
              <button className="h-14 cursor-pointer rounded-xl bg-violet-600 px-8 font-semibold text-white hover:bg-violet-900 hover:shadow-lg ">
                Book Now!
              </button>
              <button className="h-14 cursor-pointer rounded-xl border border-violet-600 bg-white  px-8 font-semibold text-violet-900 hover:bg-violet-900 hover:text-white hover:shadow-lg ">
                Get a Price
              </button>
            </div>
          </div>
          <div className="hidden md:block">
            <Image
              src="/images/hero.png"
              height={700}
              width={900}
              alt="hero img travel"
              objectFit="contain"
            />
          </div>
        </div>
      </div>
    </>
  )
}
