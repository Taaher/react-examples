import type { NextPage } from 'next'
import Head from 'next/head'

import Hero from '../components/Hero'
import Trending from '../components/Trending'
import Services from '../components/Services'
import Destination from '../components/Destination'
import Notify from '../components/Notify'

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Travelin!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Hero />
      <Trending />
      <Services />
      <Destination />
      <Notify />
    </>
  )
}

export default Home
