import { Form, LoaderFunction, useLoaderData, MetaFunction } from "remix";
import { Film, getFilms } from "~/api/filmApi";
import Card from "~/components/films/Card";

export const meta: MetaFunction = () => {
  return { title: "TZ FILM STUDIO | LIVE ", description: "tz all movie" };
};

//RENDER SERVER SIDE!
export const loader: LoaderFunction = async ({ request }) => {
  const url = new URL(request.url);
  const searchField = url.searchParams.get("search");
  return getFilms(searchField);
};

export default function FilmIndex() {
  const films = useLoaderData<Film[]>();
  console.log(films);
  return (
    <div className='p-10 lg:p-16'>
      <div className='flex font-bold justify-center items-center	 p-4'>
        <Form reloadDocument method='get'>
          <label htmlFor='search' className='hidden'>
            {" "}
            Search{" "}
          </label>
          <input
            placeholder='search'
            type='text'
            name='search'
            id='search'
            className='p-1 border-2 border-black w-3/5 font-bold'
          />
          <button className='bg-black text-white rounded p-1 m-1 w-auto hover:bg-white hover:text-black hover:border-black hover:border-2'>
            Search
          </button>
        </Form>
      </div>
      Films
      <div className='grid sm:gird-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 '>
        {films.map((film: Film) => (
          <Card
            key={film.id}
            id={film.id}
            title={film.title}
            image={film.image}
          />
        ))}
      </div>
    </div>
  );
}
