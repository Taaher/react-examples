import { LoaderFunction, useLoaderData, useParams, MetaFunction } from "remix";
import { Film, getFilmById } from "~/api/filmApi";
import invariant from "tiny-invariant";
import FilmDetail from "~/components/films/FilmDetail";

export const meta: MetaFunction = ({ data }) => {
  return {
    title: data.title,
    description: data.description,
  };
};

export const loader: LoaderFunction = async ({ params }) => {
  invariant(params.filmId, "expected params.filmId");
  const film = await getFilmById(params.filmId);
  //console.log("pre-fetching", film.title);
  return film;
};

export default function Film() {
  const film = useLoaderData<Film>();
  return (
    <div>
      <FilmDetail film={film} />
    </div>
  );
}
