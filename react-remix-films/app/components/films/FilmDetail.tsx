import { Link } from "remix";
import { Film } from "~/api/filmApi";
import FilmBanner from "./FilmBanner";

type FilmDetailProps = {
  film: Film;
};

export default function FilmDetail({ film }: FilmDetailProps) {
  return (
    <div>
      <div>
        <FilmBanner film={film} />
      </div>
      <div>
        <section className='text-gray-700 body-font overflow-hidden bg-white'>
          <div className='container px-5 py-24 mx-auto'>
            <div className='lg:w-4/5 mx-auto flex flex-wrap'>
              <img
                alt={film.title}
                className='lg:w-1/2 w-full object-cover object-center rounded border border-gray-200'
                src={film.image}
              />
              <div className='lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0'>
                <h2 className='text-sm title-font text-gray-500 tracking-widest'>
                  FILM NAME
                </h2>
                <h1 className='text-gray-900 text-3xl title-font font-medium mb-1'>
                  {film.title}
                </h1>

                <p className='leading-relaxed'>{film.description}</p>
                <div className='mt-6 items-center pb-5 border-b-2 border-gray-200 mb-5'>
                  <div className='flex justify-between space-x-6'>
                    <div>
                      <span className='title-font font-medium text-2xl text-gray-900'>
                        $58.00
                      </span>
                    </div>
                    <div>
                      <button className='flex ml-auto text-white bg-black border-0 py-2 px-6 focus:outline-none hover:text-black hover:bg-white hover:border-2 hover:border-black rounded'>
                        BUY NOW
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
