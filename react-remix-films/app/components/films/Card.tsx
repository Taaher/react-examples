import { Link } from "remix";
interface CardProps {
  title: string;
  id: string;
  image: string;
}

export default function Card({ id, title, image }: CardProps) {
  return (
    <Link
      prefetch='intent'
      to={id}
      className='cursor-pointer rounded-lg  p-2 bg-gray-200 shadow-orange-800'
    >
      <img src={image} alt={title} className='' />
      <h2 className='font-bold'>{title}</h2>
    </Link>
  );
}
