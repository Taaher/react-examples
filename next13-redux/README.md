# Nextjs 13 and Redux toolkit

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`DATABASE_URL`
For example i used [supabase](https://supabase.io/)

## Installation

Install my-project with npm

```bash
  npm install my-project
  cd my-project

  npx prisma db push
  npx prisma studio - add some field.
  npm run dev

```

## 🚀 About Me

I'm Taher Zobeydi, a full stack developer with a passion for creating user-friendly and engaging web applications.
