'use client';

import { AppDispatch, RootState } from '@/redux/store';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { increment, decrement, reset, incrementByValue } from '@/redux/features/counterSlice';

export default function CounterPage() {
    const [value, setValue] = useState<number>(0);

    const { counter } = useSelector((state: RootState) => state.counterReducer);
    const dispatch = useDispatch<AppDispatch>();

    const incrementCounter = () => dispatch(increment());
    const decrementCounter = () => dispatch(decrement());
    const resetCounter = () => dispatch(reset());
    const incrementCounterByValue = () => dispatch(incrementByValue(value));

    return <div className="h-screen flex flex-col justify-center items-center space-y-4">
        <div className="card card-compact w-96 bg-base-200 shadow-xl p-10 space-y-4">
            <input className="input input-bordered input-success w-full max-w-xs" type="number" value={value} onChange={(e) => setValue(parseInt(e.target.value))} />
            <button onClick={incrementCounterByValue} className="btn btn-outline btn-accent">Increment by Value</button>
        </div>
        <div className="card card-compact w-96 bg-base-200 shadow-xl p-10 space-y-2">
            <button onClick={incrementCounter} className="btn btn-outline btn-primary">Increment</button>
            <h2 className="text-2xl text-slate-400 text-center">{counter}</h2>
            <button onClick={decrementCounter} className="btn btn-secondary">Decrement</button>
            <button onClick={resetCounter} className="btn btn-accent ">Reset</button>
        </div>
    </div>;
}