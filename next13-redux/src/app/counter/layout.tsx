
export default function CounterLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <div data-theme="night">
            {children}
        </div>
    );
}
