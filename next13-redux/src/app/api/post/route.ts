import { NextResponse } from 'next/server';
import prisma from '@/utils/prisma';

export async function GET(request: Request) {
  const posts = await prisma.post.findMany();
  return NextResponse.json(posts, { status: 200 });
}

export async function POST(request: Request) {
  const res = await request.json();
  const { title, content } = res;
  const createdPost = await prisma.post.create({
    data: {
      title,
      content,
    },
  });
  return NextResponse.json(
    {
      sucess: true,
      text: 'Created new post',
      createdPost,
    },
    { status: 200 }
  );
}
