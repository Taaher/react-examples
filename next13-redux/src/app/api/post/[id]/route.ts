import prisma from '@/utils/prisma';
import { NextResponse } from 'next/server';

/**
 * Get post by Id
 * @param {Request} request
 * @param {{ params: { id: string } }} { params }
 * @returns {unknown}
 */
export async function GET(
  request: Request,
  { params }: { params: { id: string } }
) {
  const post = await prisma.post.findFirst({
    where: { id: params.id },
  });
  return NextResponse.json({ post }, { status: 200 });
}

export async function PUT(
  request: Request,
  { params }: { params: { id: string } }
) {
  const res = await request.json();
  const { title, content } = res;
  const post = await prisma.post.update({
    where: {
      id: params.id,
    },
    data: { title, content },
  });

  return NextResponse.json({ post }, { status: 200 });
}

export async function DELETE(
  request: Request,
  { params }: { params: { id: string } }
) {
  if (params.id === null || params.id === undefined) {
    return NextResponse.json({ msg: 'something went wrong' });
  }

  const deletedPost = await prisma.post.delete({
    where: { id: params.id },
  });
  if (deletedPost === null || deletedPost === undefined) {
    return NextResponse.json({ msg: 'User not found' });
  }

  return NextResponse.json(
    { msg: `Removed - id:${params.id}}` },
    { status: 200 }
  );
}
