
export default function PostLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <div data-theme="dracula">
            {children}
        </div>
    );
}
