'use client';
import { getAllPost } from '@/redux/features/postSlice';
import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import PostCard from '@/components/post/PostCard';
export default function PostPage() {
    const { posts, status, errMessage } = useAppSelector(state => state.postSlice);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(getAllPost());
    }, [dispatch]);

    if (status === "loading") return <h2>Loading!</h2>;
    if (status === "error") return <h2>`Error! ${errMessage}`</h2>;
    return (
        <div className="h-screen">
            <div className="flex justify-center items-center h-full flex-col space-y-2">
                {posts && posts?.map((post: any) => <PostCard key={post.id} id={post.id} title={post.title} content={post.content} />)}
            </div>
        </div >
    );
}