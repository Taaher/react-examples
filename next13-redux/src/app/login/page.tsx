'use client';

import { useState } from 'react';
import { login, logOut } from '@/redux/features/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '@/redux/store';

export default function Login() {
    const [username, setUsername] = useState<string>("");
    const auth = useSelector((state: RootState) => state.authReducer.value);
    const dispatch = useDispatch<AppDispatch>();

    console.log(auth);
    const onClickLogin = () => {
        dispatch(login(username));
    };
    const onClickToggle = () => { };
    const onClickLogOut = () => {
        dispatch(logOut());
    };
    return (
        <div>
            {auth.username ? `Welcome ${auth.username}` : "Please Login"}
            <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
            <h2> Login is Ready! </h2>
            <button onClick={onClickLogin} className="btn btn-primary"> Login </button>
            <button onClick={onClickLogOut} className="btn btn-secondary"> Logout </button>
        </div>
    );
}