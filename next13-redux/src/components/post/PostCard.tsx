import Link from 'next/link';
type PostCardPropsType = { id: string, title: string, content: string; };
export default function PostCard({ id, title, content }: PostCardPropsType) {
    return (
        <div className="card w-96 bg-secondary text-primary-content">
            <div className="card-body">
                <h2 className="card-title">{title}</h2>
                <p>{content}</p>
                <div className="card-actions justify-end">
                    <Link href={`/post/${id}`} className="btn btn-primary">Learn More..</Link>
                </div>
            </div>
        </div>
    );
}