import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

type Post = {
  title: string;
  content: String;
};
type InitialStateType = {
  posts: Post[];
  errMessage: string;
  status: 'error' | 'loading' | 'success';
};
const initialState = {
  posts: [],
  errMessage: '',
  status: 'loading',
} as InitialStateType;

//get all post
export const getAllPost = createAsyncThunk('posts/getAllPost', async () => {
  console.log('get all post');
  const posts = await fetch('api/post');
  return posts.json();
});

const postSlice = createSlice({
  name: 'post',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    //run loading
    builder.addCase(getAllPost.pending, (state) => {
      state.status = 'loading';
    });
    //get all post success
    builder.addCase(getAllPost.fulfilled, (state, action) => {
      state.status = 'success';
      state.posts = action.payload;
    });
    //get all post failed
    builder.addCase(getAllPost.rejected, (state, action) => {
      state.status = 'error';
      state.errMessage = action.error.message || 'the server is not responding';
    });
  },
});
// export const {} = postSlice.actions;
export default postSlice.reducer;
