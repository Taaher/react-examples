import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type AuthStateType = {
  isAuth: boolean;
  username: string;
  uid: string;
  isModerator: boolean;
};

type InitialStateType = {
  value: AuthStateType;
};

const initialState = {
  value: {
    isAuth: false,
    username: '',
    uid: '',
    isModerator: false,
  } as AuthStateType,
} as InitialStateType;
const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logOut: () => {
      return initialState;
    },
    login: (state, action: PayloadAction<string>) => {
      return {
        value: {
          isAuth: true,
          username: action.payload,
          uid: 'dewdwedwedwewd',
          isModerator: false,
        },
      };
    },
  },
});

export const { logOut, login } = auth.actions;
export default auth.reducer;
