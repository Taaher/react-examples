import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type InitialStateType = {
  counter: number;
};
const initialState: InitialStateType = {
  counter: 0,
};

const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state: InitialStateType) => {
      state.counter += 1;
    },
    decrement: (state: InitialStateType) => {
      state.counter -= 1;
    },
    reset: (state: InitialStateType) => {
      state.counter = 0;
    },
    incrementByValue: (
      state: InitialStateType,
      action: PayloadAction<number>
    ) => {
      state.counter += action.payload;
    },
  },
});
export const { increment, decrement, reset, incrementByValue } =
  counterSlice.actions;
export default counterSlice.reducer;
