import { configureStore } from '@reduxjs/toolkit';
import authReducer from './features/authSlice';
import counterReducer from './features/counterSlice';
import postSlice from './features/postSlice';

export const store = configureStore({
  reducer: {
    postSlice,
    authReducer,
    counterReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
